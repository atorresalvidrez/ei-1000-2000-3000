%
O0000(REGISTRO AXIALES)
(DATE=DD-MM-YY - 05-03-20 TIME=HH:MM - 15:37)
(MCX FILE - C:\USERS\MIRIAM\ONEDRIVE\DOCUMENTOS EI\MECANICA MEX-MID\DISE�O MECANICO\SERIE EI\EI 2000-3000\MASTERCAM\AXIALES EI2000.MCX-8)
(NC FILE - C:\USERS\MIRIAM\ONEDRIVE\DOCUMENTOS EI\MECANICA MEX-MID\DISE�O MECANICO\SERIE EI\EI 2000-3000\MASTERCAM\CODIGO\REGISTRO AXIALES.NC)
(MATERIAL - ALUMINUM INCH - 2024)
(Z(0) PLANO SUPERIOR DE LA PIEZA)
(ALINEAR EN EJE X,Y)
(CLARO MINIMO DE .125)
(4 X 3 X 1.5 DOS PIEZAS)
( T6 | 1/2 FLAT ENDMILL | H6 )
( T5 | 1/2 FLAT ENDMILL | H5 )
N100 G20
N102 G0 G17 G40 G49 G80 G90
N104 T6 M6
N106 G0 G90 G54 X-.5 Y-.49 S1000 M3
G04 X1
S3000 M3
N108 G43 H6 Z.25 M8
N110 Z.2
N112 G1 Z-.05 F25.
N114 X.395 Y-1.385
N116 Y-1.605
N118 X3.605
N120 Y-1.385
N122 X.395
N124 X.27 Y-1.26
N126 Y-1.73
N128 X3.73
N130 Y-1.26
N132 X.27
N134 X.02 Y-1.01
N136 Y-1.98
N138 X3.98
N140 Y-1.01
N142 X.02
N144 X-.23 Y-.76
N146 Y-2.23
N148 X4.23
N150 Y-.76
N152 X-.23
N154 G0 Z.25
N156 M9
N158 M5
N160 G91 G28 Z0.
N162 M01
N164 T5 M6
N166 G0 G90 G54 X-.25 Y-2.25 S1000 M3
G04 X1
S3000 M3
N168 G43 H5 Z.25
N170 Z.2
N172 G1 Z-.05 F15.
N174 Y-.74
N176 X4.25
N178 Y-2.25
N180 X-.25
N182 G0 Z.25
N184 M5
N186 G91 G28 Z0.
N188 G28 X0. Y0.
N190 M30
%
